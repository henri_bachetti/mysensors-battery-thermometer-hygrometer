
// Enable debug prints to serial monitor
#define MY_DEBUG

// Enable and select radio type attached
#define MY_RADIO_NRF24
//#define MY_RADIO_RFM69

#define MY_RF24_CE_PIN        7
#define MY_RF24_CS_PIN        8

#define SLEEP_TIME            900000L
#define COMPARE_TEMP          0
#define VREF                  1.099

#include <MySensors.h>  
#include <Adafruit_SHT31.h>

#define CHILD_ID_TEMP         0
#define CHILD_ID_HUM          1

Adafruit_SHT31 sht31 = Adafruit_SHT31();

struct batteryCapacity
{
  float voltage;
  int capacity;
};

const batteryCapacity remainingCapacity[] = {
  4.20,   100,
  4.10,   96,
  4.00,   92,
  3.96,   89,
  3.92,   85,
  3.89,   81,
  3.86,   77,
  3.83,   73,
  3.80,   69,
  3.77,   65,
  3.75,   62,
  3.72,   58,
  3.70,   55,
  3.66,   51,
  3.62,   47,
  3.58,   43,
  3.55,   40,
  3.51,   35,
  3.48,   32,
  3.44,   26,
  3.40,   24,
  3.37,   20,
  3.35,   17,
  3.27,   13,
  3.20,   9,
  3.1,    6,
  3.00,   3,
};

const int ncell = sizeof(remainingCapacity) / sizeof(struct batteryCapacity);

void before()
{
  int mode_1;
  (mode_1, 1);
  Serial.print("MYSENSORS SHT31D Temperature Humidity sensor: ");
  // Startup up the SHT31 library
  Serial.println(F("Start SHT31"));
  if (!sht31.begin(0x44)) {
    Serial.println(F("Couldn't find SHT31"));
  }
  Serial.println("OK");
}

void setup()  
{ 
  Serial.begin(115200);
  Serial.print("Setup");
  Serial.println(" OK");
}

void presentation() {
  Serial.print("Presentation");
  // Send the sketch version information to the gateway and Controller
  sendSketchInfo("SHT31D Temperature & Humidity Sensor", "1.1");

  // Present all sensors to controller
  present(CHILD_ID_TEMP, S_TEMP);
  present(CHILD_ID_HUM, S_HUM);
  Serial.println(" OK");
}

unsigned int getBatteryCapacity(void)
{
//  float voltage = (1023 * VREF) / analogReadReference();
  analogReference(INTERNAL);
  analogRead(0);
  delay(1);
  unsigned int adc = analogRead(0);
#ifdef MY_DEBUG
  Serial.print("ADC: ");
  Serial.println(adc);
#endif
  float voltage = adc * VREF / 1023 / 0.248;
#ifdef MY_DEBUG
  Serial.print("VCC: ");
  Serial.println(voltage, 3);
#endif
  for (int i = 0 ; i < ncell ; i++){
#ifdef MY_DEBUG
    Serial.print(i);
    Serial.print(" : ");
    Serial.print(remainingCapacity[i].voltage);
    Serial.print(" | ");
    Serial.println(remainingCapacity[i].capacity);
#endif
    if (voltage > remainingCapacity[i].voltage) {
      return remainingCapacity[i].capacity;
    }
  }
  return 0;
}

void readSHT31D(void)
{
  MyMessage tempMsg(0,V_TEMP);
  MyMessage humMsg(0,V_HUM);
  static float lastTemperature;
  
  float temperature = sht31.readTemperature();
  float humidity = sht31.readHumidity();

  #if COMPARE_TEMP == 1
  if (lastTemperature != temperature && !isnan(temperature)) {
  #else
  if (!isnan(temperature)) {
  #endif
    // Send in the new temperature
    send(tempMsg.setSensor(CHILD_ID_TEMP).set(temperature, 1));
    send(humMsg.setSensor(CHILD_ID_HUM).set(humidity, 1));
    // Save new temperatures for next compare
    lastTemperature = temperature;
    Serial.print("transmitted temperature OK: ");
    Serial.print(temperature, 2);
    Serial.println("°C");
    Serial.print("transmitted humidity OK: ");
    Serial.print(humidity, 2);
    Serial.println("%");
    delay(10);
  }
}

void loop()     
{
  readSHT31D();
  sleep(SLEEP_TIME);
  int batteryLevel = getBatteryCapacity();
  sendBatteryLevel(batteryLevel);
  Serial.print("transmitted battery level OK: ");
  Serial.print(batteryLevel);
  Serial.println("%");
}

